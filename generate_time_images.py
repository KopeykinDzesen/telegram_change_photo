from utils import convert_time_to_string
import cv2
import numpy
from datetime import datetime, timedelta


def get_black_background():
    return numpy.zeros((300, 300, 3))


start_time = datetime(2019, 1, 1)
end_time = start_time + timedelta(days=1)


def generate_image_with_text(hours_minutes, seconds):
    image = get_black_background()
    font = cv2.FONT_HERSHEY_DUPLEX
    cv2.circle(image, (150, 150), 125, (255, 255, 255), -150)
    cv2.putText(image, hours_minutes, (35, 170), font, 2.5, (0, 0, 0), 5)
    cv2.putText(image, seconds, (120, 230), font, 1.5, (0, 0, 0), 3)
    return image


while start_time < end_time:
    hours_minutes, seconds = convert_time_to_string(start_time)
    image = generate_image_with_text(hours_minutes, seconds)
    cv2.imwrite('time_images/{text}.jpeg'.format(text=f'{hours_minutes}:{seconds}'), image)
    start_time += timedelta(seconds=5)
