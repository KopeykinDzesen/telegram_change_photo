def convert_time_to_string(dt):
    return dt.strftime("%H:%M"), dt.strftime("%S")


def time_has_changed(prev_time, dt_now):
    return convert_time_to_string(dt_now) != prev_time


def time_is_valid(dt_now):
    return dt_now.second % 5 == 0
