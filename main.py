import time
from datetime import datetime

import pytz

from config import api_hash, api_id
from telethon import TelegramClient, sync
from telethon.tl.functions.photos import DeletePhotosRequest, UploadProfilePhotoRequest
from utils import convert_time_to_string, time_has_changed, time_is_valid

client = TelegramClient("Volk", api_id, api_hash)
client.start()

prev_time = ""

while True:
    time.sleep(0.5)
    dt_now = datetime.now().astimezone(pytz.timezone("Europe/Minsk"))

    if time_has_changed(prev_time, dt_now) and time_is_valid(dt_now):
        prev_time_hours_and_minutes, prev_time_seconds = convert_time_to_string(dt_now)
        client(DeletePhotosRequest(client.get_profile_photos("me")))
        file = client.upload_file(f"time_images/{prev_time_hours_and_minutes}:{prev_time_seconds}.jpeg")
        client(UploadProfilePhotoRequest(file))
